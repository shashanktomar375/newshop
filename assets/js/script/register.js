$(document).ready(function() {

  // Validation de l'inscription
  $('#register_form').submit(function(e) {
    $('.register_info').remove();
    $('.register_error').remove();

    var error = 0;

    var email = $('input[id=register_email]');
    var tel = $('input[id=register_tel]');
    var username = $('input[id=register_username]');
    var password = $('input[id=register_password]');
    var confirm = $('input[id=register_confirm]');

    if (email.val() == "") {
      error++;
      email.after('<span class="register_error text-warning">Champ vide</span>');
    } else if (email.val().match(/^[a-z0-9-_+.]+@([a-z0-9-]+\.)+[a-z]+$/i) == null){
      error++;
      email.after('<span class="register_info text-info">Format de l\'adresse email : exemple@exemple.be</span>');
    }

    if (tel.val() == "") {
      error++;
      tel.after('<span class="register_error text-warning">Champ vide</span>');
    } else if (tel.val().match(/^[0-9]+(( |-)[0-9]+)*$/i) == null) {
      error++;
      tel.after('<span class="register_error text-warning">Téléphone invalide</span>');
    }

    if (username.val() == "") {
      error++;
      username.after('<span class="register_error text-warning">Champ vide</span>');
    } else if (username.val().match(/^[a-z]+(( |-)[a-z]+)*$/i) == null) {
      error++;
      username.after('<span class="register_info text-warning">Nom d\'utilisateur invalide</span>');
    }

    if (password.val() == "") {
      error++;
      password.after('<span class="register_error text-warning">Champ vide</span>');
    } else if (confirm.val() == "") {
      error++;
      confirm.after('<span class="register_error text-warning">Champ vide</span>');
    } else if (password.val() != confirm.val()) {
      error++;
      confirm.after('<span class="register_error text-warning">Les deux mots de passe sont différents</span>');
    }

    if (error == 0) {
      console.log("Je suis inscrit");
      register(username.val(), password.val(), email.val(), tel.val());
    }

    e.preventDefault();
  });
});

function register(username, password,email, phone) {
	$.ajax({
		url: "/register",
		type: "POST",
		dataType: "json",
		data: {
			username: username,
			password: password,
			email: email,
			phone: phone
		},
		success: function(res) {
			console.log("Verification de tout ces élements au niveau du serveur ensuite il faudra envoyer un mail à l'utilisateur pour la confirmation");
		},
		error: function(err) {
			console.log("Une erreur s'est produite dans l'ajax de la méthode register() dans le script.js");
		}
	});
}
