$(document).ready(function() {

  //Validation du Login
  $('#login_form').submit(function(e) {
    e.preventDefault();
    $("#messageAlert").html('');

    var email = $('input[id=login_email]');
    var pwd = $('input[id=login_password]');

    if(email.val() == "") {
      if(pwd.val() == "") {
        $("#messageAlert").html('<div class="alert alert-warning">Veuillez entrer l\'adresse email et le mot de passe</div>');
      } else {
        $("#messageAlert").html('<div class="alert alert-warning">Veuillez entrer l\'adresse email</div>');
      }
    } else if (pwd.val() == "") {
      $("#messageAlert").html('<div class="alert alert-warning">Veuillez entrer le mot de passe</div>');
    } else {
      console.log("Je suis connecté");
      connectUser(email.val(), pwd.val());
    }

  });
});

function connectUser(email, password) {
	$.ajax({
		url: "/login",
		type: "POST",
		dataType: "json",
		data: {
			email: email,
			password: password
		},
		success: function(res) {
      console.log(res);
      sessionStorage.user = JSON.stringify(res);
      window.location.href = "/";
		},
		error: function(err) {
			console.log("Une erreur s'est produite dans l'ajax de la méthode connectUser() dans login.js")
		}
	})
}
