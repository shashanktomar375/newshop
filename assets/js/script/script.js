$(document).ready(function(){
    /*NOTRE APPLICATION*/

    //Pour effectuer une enchère sur un article
    $("#auction").on('click',function (){
      var id = $(this).attr('id');
      var montant = $('#montant').val();
      auction(id, montant);
    });//fin enchère
    
    //Boutton de recherche /search
    $("#search").click(function(){
      var search_element = $.trim($("#search_element").val());
      if(search_element.length!=0)
        searching(search_element);
    });//fin de recherche
  

    /*//Cliquer sur le bouton pour se connecter /authentification
    $("#login").submit(function(event){
      event.preventDefault();
      var mail = $("#mail_login").val();
      var password = $("#password_login").val();
      if(mail.length==0||password.length==0)console.log("Un des champs n'est pas complété");
      else connectUser(mail,password);
    });//fin authentification*/


    //Cliquer sur bouton Envoyer , pour envoyer le mdp sur mail
    $("#lost_password").submit(function(event){
      event.preventDefault();
      var email = $.trim($("#email_password_lost").val());
      if(email.length!=0)
        sendMailForPassword(email);
    });//fin authentification


    /*//Cliquer sur boutton M'inscrire
    $("#register").submit(function(event){
      event.preventDefault();
      var username = $("#username_register").val();
      var password = $("#password_register").val();
      var password_confirm = $("#password_confirm_register").val();
      var email = $("#email_register").val();
      var phone = $("#phone_register").val();
      if(password != password_confirm)console.log("Les mots de passe ne concordent pas!");
      if($.trim(username).length==0||$.trim(password).length==0||$.trim(email).length==0||$.trim(phone).length==0)
        console.log("Un des champs n'est pas complété");
      else register(username,password,email,phone);
    })//fin d'inscription*/




    //Récupération dans un tableau de toute les images
    var files;
    var uploadArray=[];
    $("#file_image").on('change',function(){
      files = $(this).get(0).files;
      if(files.length>0){
        for(var i=0; i < files.length; i++){
          uploadArray.push(files[i]);
        } 
      }
    });//Fin de récupération    

    //Cliquer sur boutton pour confirmer l'ajout de l'article /addGood 
    $("#confirm").on('click',function(){
      var title = $.trim($("#title").val());
      var category = $("#category option:selected").text();
      var price = $("#price").val();
      var description = $("#description").val();
      if(title.length!=0)
        saleItem(title,price,category,description,uploadArray);
      else 
        console.log("Veuillez remplir le champ titre");
    });//Fin de l'ajout de l'article


    
    
});

function auction(idAuction, montant) {
  $.ajax({
      url: "/addAuction",
      method: "POST",
      dataType: "JSON",
      data: {id: idAuction, price : montant},
      success: function (data) {
          console.log("success");
      },
      error: function (data) {
      }
  });
}

function saleItem(title,price,category,description,uploadArray){
  $.ajax({
    url:"/addGood",
    type: "POST",
    dataType: "json",
    data: {
      title:title,
      price:price,
      description:description,
      category:category,
      uploadArray:uploadArray
    },//uploadArray est un tableau 
    processData:false,
    contentType: false,
    cache:false,
    success:function(res){
      console.log("Ok");
    },
    error: function(err){
      console.log("Une erreur s'est produite dans l'ajax de la méthode saleItem() dans le script.js");
    }
  });
}


function connectUser(mail,password){
  $.ajax({
    url:"/authentication",
    type:"POST",
    dataType:"json",
    data:{mail:mail,password:password},
    success:function(res){
      console.log("Diriger l'utilisateur dans sa page respective");
    },
    error: function(err){
      console.log("Une erreur s'est produite dans l'ajax de la méthode connectUser() dans le script.js")
    }
  })
}


function sendMailForPassword(email){
  $.ajax({
    url:"/authentication",
    type:"POST",
    dataType:"json",
    data:{email:email},
    success:function(res){
      var result = JSON.parse(res);
      if(!result){
        $(".error").html("Votre compte n'existe pas !");
      }else{
        $(".error").css("color","green");
        $(".error").html("Un mail vous a été envoyé avec votre mot de passe");
      }
    },
    error: function(err){
      console.log("Une erreur s'est produite dans l'ajax de la méthode sendMailPassword() dans le script.js")
    }
  })
}


function register(username,password,email,phone){
  $.ajax({
    url:"/authentication",
    type:"POST",
    dataType:"json",
    data:{
      username:username,
      password:password,
      email:email,
      phone:phone},
      success:function(res){
        console.log("Verification de tout ces élements au niveau du serveur ensuite il faudra envoyer un mail à l'utilisateur pour la confirmation");  
      },
      error: function(err){
        console.log("Une erreur s'est produite dans l'ajax de la méthode register() dans le script.js");
      }
    });


    function searching(element){
      $.ajax({
        url:"/search",
        type:"POST",
        dataType:"json",
        data:{element_search:element},
        success:function(res){
           console.log("Redirection vers une page de recherche");
        },
        error: function(err){
            console.log("Une erreur s'est produite dans l'ajax de la méthode register() dans le script.js");
        }
      });
    }

}