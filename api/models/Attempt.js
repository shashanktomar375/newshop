/**
 * Attempt
 *
 * @module      :: Model
 * @description :: Tracks login attempts of users on your app.
 * @docs        :: http://waterlock.ninja/documentation
 */

module.exports = {
  meta: {
    schemaName: 'trusty_db'
 },
  attributes: require('waterlock').models.attempt.attributes({
    
    /* e.g.
    nickname: 'string'
    */
    
  })
};