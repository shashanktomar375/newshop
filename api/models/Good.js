/**
 * Good.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'goods',
  meta: {
    schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {

    title: {
      type: 'string'
      //add size
    },

    description: {
      type: 'string'
    },

    id_seller: {
      model: 'user',
      columnName: 'seller_id',
      required: true
    },

    id_buyer: {
      model: 'user',
      columnName: 'buyer_id'
    },

    starting_price: {
      type: 'float'
    },

    category_id: {
      model: 'category',
      columnName: 'category_id'
    },

    good_state: {
      type: 'character',
      enum: ['S', 'O', 'P', 'D'],
      defaultsTo: 'O'
    },

    end_date: {
      type: 'date'
    },

    start_date: {
      type: 'date'
    }
  },

  //------------------------------------METHODES------------------------------------
  /**
   * Affiche tous les goods
   */
  getAll: function () {
    return Good.find();
  },

  getById: function (id) {
    return Good.findOne({ id: id });
  },

  getOnSale: function () {
    return Good.find({ good_state:'O' });
  },

  changeState: function (goodID, newState) {
    if (newState != 'O' || newState != 'S' || newState != 'P' || newState != 'D') {
      return null;
    } else {
      if (this.attributes.good_state == 'D' || this.attributes.good_state == 'S') {
        return null;
      }
      return Good.update({id: id}, {good_state: newState});
    }

  },

  getByName: function (name) {
    return Good.find({ name: name });
  },

  getByPriceRange: function (min, max) {
    return Good.find({ starting_price: { '>': min, '<': max } });
  },

  getByCategory: function(categoryID){
    return Good.find({category_id: categoryID});
  },

  addGood: function (title, description, price, categoryID) {
    //------------------------!!!!!!!!!!!-----------TODO dates & id seller---------------------!!!!!!!!!!!!------------------
    return Good.create({title: title, description: description, id_seller: 1, category_id: categoryID, starting_price: price, good_state: 'O'});
  },

  getGoodsByOwner: function(id){
    return Good.find({id_seller: id});
  },
}
