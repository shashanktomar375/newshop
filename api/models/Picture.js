/**
 * Picture.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: 'pictures',
  meta: {
    schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {

    url: {
      type: 'string'
    },

    //Add size max ?
    title: {
      type: 'string'
    },

    good: {
      model: 'good',
      columnName: 'good_id'
    }
  },
  //------------------------------------METHODES------------------------------------
  getPicturesOf: function(goodID){
    return Picture.find({good: goodID});
  },

  addPicture: function(uploadArray, goodID){
    for(var i = 0; i < uploadArray.length; i++){
      uploadArray[i].upload({
          dirname: '../../assets/images',
          saveAs: goodID+'_'+i
      }, function(err, file){
        Picture.create({url: file, title: file.name, good: goodID});
      });
    }
  }
};

