/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'comments',
  meta: {
     schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: {
    id_user_commented: {
      model:'user',
      columnName: 'user_commented_id',
      required:true
    },

    id_author: {
      model:'user',
      columnName: 'author_id'
    },

    id_good: {
      model:'good',
      columnName: 'good_id'
    },

    comment: {
      type: 'string'
    },

    score: {
      type: 'integer',
      enum : [0,1,2,3,4,5],
      defaultsTo : 0
    }
  },
  //------------------------------------METHODES------------------------------------
  getAllCommentsByGoodId: function(goodID){
    return Good.find({id_good: goodID});
  },

  getAllCommentsByUserCommented: function(userID){
    return Good.find({id_user_commented: userID});
  },

  getAllCommentsByAuthorId: function(authorID){
    return Good.find({id_author: authorID});
  },

  getAllCommentsForGoodByScore: function(goodID,score){
    return Good.find({id_good: goodID, score: score});
  },

  addComment: function(userID, authorID, goodID, comment, score){
    return Comment.create({id_user_commented: userID, id_author: authorID, id_good: goodID, comment: comment, score: score});
  }
};

