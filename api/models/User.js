/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'users',
  meta: {
     schemaName: 'trusty_db'
  },
  //------------------------------------ATTRIBUTS------------------------------------
  attributes: require('waterlock').models.user.attributes({

    nat_reg_number : {
      type: 'string'
    },

    first_name : {
      type: 'string'
    },

    last_name: {
      type: 'string' 
    },

    address: {
      type: 'string'
    },

    phone: {
      type: 'string'
    },

    login: {
      type: 'string'
    },

    birth_date: {
      type: 'date'
    },

    registration_date: {
      type: 'date',
      defaultsTo: function () {
        return new Date();
      }
    },

    confirmed: {
      type : 'boolean',
      defaultsTo : false,
    }

  }),
  //------------------------------------METHODES------------------------------------
  getAllBoughtGoods: function(){
    return Good.find({id_buyer: this.attributes.id}).where({good_state: { '!': 'O'}});
  },

  getAllSoldGoods: function(){
    return Good.find({id_seller: this.attributes.id}).where({good_state: { '!': 'O'}});
  },

  getAllOnSaleGoods: function(){
    return Good.find({id_seller: this.attributes.id}).where({good_state: 'O'});
  },

  getUserByName: function(name){
    return User.findOne().where({or: [{first_name: {like: '%'+name}}, {last_name: {like: '%'+name}}]});
  },

  getUserById: function(id){
    return User.findOne({ id: id});
  }
};

