/**
 * GoodController
 *
 * @description :: Server-side logic for managing Goods
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    //------------------------!!!!!!!!!!!-----------TODO buyGood---------------------!!!!!!!!!!!!------------------
    getAll: function (req, res) {
        Good.getAll().then(function (results) {
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getById: function (req, res) {
        const id = req.params.id;
        Good.getById(id).then(function (result) {
            if (!result) {
                res.view('404');
            } else {
                res.view('good',{good: result})
            }
        }).catch(function () {
            res.view('500');
        });
    },

    getGoodsByCategory: function(req, res){
        var categoryID = req.params.categorie;
        var category;
        var pictures;
        Category.getCategoryById(categoryID).then(function(categoryFound){
            if(!categoryFound){
                res.view('404');
            }else{
                category = categoryFound;
                return Good.getByCategory(categoryID);
            }
        }).then(function(goods){
            if(!goods){
                res.view('404');
            }else{
                res.view('goods',{goods: goods, categorie : category.name})
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getOnSale: function (req, res) {
        var id = req.param('id');
        var goodsOnSale;
        Good.getOnSale().then(function (goods) {
            if (!goods) {
                res.view('404');
            } else {
                goodsOnSale = goods;
                return Picture.getPicturesOf(id);
            }
        }).then(function(pictures){
            if(!pictures){
                res.view('404');
            }else{
                res.view('goods',{goods: goodsOnSale, categorie: "Toutes les catégories", picture: pictures[0]});
            }
        }).catch(function (err) {
            return res.serverError( err );
        });
    },

    //je pense que ceci ne devrait pas etre dans un controller pck 
    // ca ne sera pas utilisé par quelqu'un directement mais plutot par le backend
    // TODO transformer en service !!! 
    changeState: function (req, res) {
        const goodID = req.param('goodID');
        const newState = req.param('goodState');
        Good.changeState(goodID, newState).then(function (good) {
            if (!good) {
                res.view('404');
            } else {
                res.view('good',{good: good});
            }
        }).catch(function () {
            res.view('500');
        });
    },

    getByName: function (req, res) {
        const name = req.param('name');
        //check si string?
        Good.getByName(name).then(function (results) {
            if (!results) {
                res.view('404');
            } else {
                res.ok(JSON.stringify(results));
            }
        }).catch(function () {
            res.view('500');
        });
    },

    getByPriceRange: function (req, res) {
        const min = req.param('min');
        const max = req.param('max');
        if(min < 0 || max < 0 || max < min){
            res.badRequest("Le minimum et maximum doivent être positifs.");
        }else{
            Good.getByPriceRange(min,max).then(function (results) {
                if (!results) {
                    res.view('404');
                } else {
                    res.ok(JSON.stringify(results));
                }
            }).catch(function () {
                res.view('500');
            });
        }
    },

    addGood: function (req, res) {
        const title = req.param('title');
        const description = req.param('description');
        const price = req.param('price');
        const categoryID = req.param('categoryID');
        const uploadArray = req.param('uploadArray');

        Good.addGood(title, description, price, categoryID).then(function(good){
            if(!good){
                res.view('404');
            }else{
                return Picture.addPicture(uploadArray, good.id);
            }
        }).then(function (pictures) {
            if (!pictures) {
                res.view('404');
            } else {
                return Category.getAllCategories();
            }
        }).then(function(categories){
            if(!categories){
                res.view('404');
            }else{
                res.view('addgood', {categories: categories});
            }
        }).catch(function () {
            res.view('500');
        });
    },

    getGoodsByOwner: function(req,res){
        
        Good.getGoodsByOwner(req.session.user.id).then(function(mygoods){
            res.view('mygoods',{goods:mygoods});
        });
    }
};

