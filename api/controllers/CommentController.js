/**
 * CommentController
 *
 * @description :: Server-side logic for managing Comments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getAllCommentsByGoodId: function(req, res){
        const goodID = req.param('goodID');
        Comment.getAllCommentsByGoodId(goodID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getAllCommentsByUserCommented: function(req, res){
        const userID = req.paramt('userID');
        Comment.getAllCommentsByUserCommented(userID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        })
    },

    getAllCommentsByAuthorId: function(req, res){
        const authorID = req.param('authorID');
        Comment.getAllCommentsByAuthorId(authorID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getAllCommentsForGoodByScore: function(req, res){
        const goodID = req.param('goodID');
        const score = req.param('score');
        if(score < 1 || score > 5){
            //Renvoie code 400 - front affiche le msg
            res.badRequest("Le score ne peut pas être inférieur à 1 et supérieur à 5.");
        }else{
            Comment.getAllCommentsForGoodByScore(goodID, score).then(function(results){
                if(!results){
                    res.view('404');
                }else{
                    res.ok(JSON.stringify(results));
                }
            }).catch(function(){
                res.view('500');
            }); 
        }
    },

    addComment: function(){
        const userID = req.param('userID');
        const authorID = req.param('authorID');
        const goodID = req.param('goodID');
        const comment = req.param('comment');
        const score = req.param('score');
        //check comment?
        if(score < 1 || score > 5){
            //Renvoie code 400 - front affiche le msg
            res.badRequest("Le score ne peut pas être inférieur à 1 et supérieur à 5.");
        }else{
            Comment.addComment(userID, authorID, goodID, comment, score).then(function(results){
                if(!results){
                    res.view('404');
                }else{
                    res.ok(JSON.stringify(results));
                }
            }).catch(function(){
                res.view('500');
            })
        }
    }
};

