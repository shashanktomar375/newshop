/**
 * CategoryController
 *
 * @description :: Server-side logic for managing Categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getAllCategories: function(req, res){
        Category.getAllCategories().then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(results);
            }
        }).catch(function(){
            res.view('500');
        })
    },

    getCategoryById: function(req, res){
        const categoryID = req.param('id');
        Category.getCategoryById(categoryID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(results);
            }
        }).catch(function(){
            res.view('500');
        })
    }
};

