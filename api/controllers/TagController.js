/**
 * TagController
 *
 * @description :: Server-side logic for managing Tags
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    getObjectTags: function(req,res){
        const goodID = req.param('goodID');
        Tag.getObjectTags(goodID).then(function(results){
            if(!results){
                //Object a d'office un tag? si non, à changer
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        })
    },

    addTagToObject: function(req, res){
        const goodID = req.param('goodID');
        const tagName = req.param('tagName');
        Tag.addTagToObject(goodID, tagName).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        })
    }
};

