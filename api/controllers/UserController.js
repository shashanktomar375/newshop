/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = require('waterlock').actions.user({
    getAllBoughtGoods: function (req, res) {
        User.getAllBoughtGoods().then(function (results) {
            if (!results) {
                res.view('404');
            } else {
                res.ok(JSON.stringify(results));
            }
        }).catch(function () {
            res.view('500');
        })
    },

    getAllSoldGoods: function (req, res) {
        User.getAllSoldGoods().then(function (results) {
            if (!results) {
                res.view('404');
            } else {
                res.ok(JSON.stringify(results));
            }
        }).catch(function () {
            res.view('500');
        })
    },

    getAllOnSaleGoods: function (req, res) {
        User.getAllOnSaleGoods().then(function (results) {
            if (!results) {
                res.view('404');
            } else {
                res.ok(JSON.stringify(results));
            }
        }).catch(function () {
            res.view('500');
        })
    },

    getUserByName: function (req, res) {
        const name = req.param('name');
        User.getUserByName(name).then(function (results) {
            if (!results) {
                res.view('404');
            } else {
                res.ok(JSON.stringify(results));
            }
        }).catch(function () {
            res.view('500');
        })
    },
    conf: function (req, res, next) {
        var app = sails.hooks.http.app;
        var bodyParser = require('body-parser');
        var passport = require('passport');
        var SamlStrategy = require('passport-saml').Strategy;


        passport.use(new SamlStrategy(
            {
                path: "/confirmUser/callback",
                entryPoint: "https://www.e-contract.be/eid-idp/protocol/saml2/post/auth-ident",
                cert: "MIIFPzCCBCegAwIBAgIJALvvTI5tCPI9MA0GCSqGSIb3DQEBCwUAMIG0MQswCQYDVQQGEwJVUzEQMA4GA1UECBMHQXJpem9uYTETMBEGA1UEBxMKU2NvdHRzZGFsZTEaMBgGA1UEChMRR29EYWRkeS5jb20sIEluYy4xLTArBgNVBAsTJGh0dHA6Ly9jZXJ0cy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5LzEzMDEGA1UEAxMqR28gRGFkZHkgU2VjdXJlIENlcnRpZmljYXRlIEF1dGhvcml0eSAtIEcyMB4XDTE3MDMyMTEwMTAwMFoXDTIwMDMyMTEwMTAwMFowPzEhMB8GA1UECxMYRG9tYWluIENvbnRyb2wgVmFsaWRhdGVkMRowGAYDVQQDExFlaWQuZS1jb250cmFjdC5iZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMQTQADLkd61VyXdLM/jPuQC/nLL/pK18kFc9h3vkxnpOvhnHZGOUwtSGY2/p32xO7rkfLZzaVxFQYm314cP2NFTMMI1puPCMmyICbNpcnIvG2eek+G1eFz4KrNPg+kuMvKdek5RjKPxDi6ZWCdgqxL2zf5q5QfkhypSe3gLNOv2u4AUmWjN/HLEu1R+N+J7X4sIYvJxh26FI4PyzvkkY7TG2vVcOYJ53J1579dARaecBaa6SpuwOBZbHKPnTmik3kP3DHIVifPunEYh6aAghbOYEYQai7TQZsJPHNesvOqaS9Z4nGmkza28Ublc1pvzWeNR4gushVUzFSuW4hJRD4MCAwEAAaOCAcYwggHCMAwGA1UdEwEB/wQCMAAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMA4GA1UdDwEB/wQEAwIFoDA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmdvZGFkZHkuY29tL2dkaWcyczEtNDQ2LmNybDBdBgNVHSAEVjBUMEgGC2CGSAGG/W0BBxcBMDkwNwYIKwYBBQUHAgEWK2h0dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29kYWRkeS5jb20vcmVwb3NpdG9yeS8wCAYGZ4EMAQIBMHYGCCsGAQUFBwEBBGowaDAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZ29kYWRkeS5jb20vMEAGCCsGAQUFBzAChjRodHRwOi8vY2VydGlmaWNhdGVzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkvZ2RpZzIuY3J0MB8GA1UdIwQYMBaAFEDCvSeOzDSDMKIz1/tss/C0LIDOMDMGA1UdEQQsMCqCEWVpZC5lLWNvbnRyYWN0LmJlghV3d3cuZWlkLmUtY29udHJhY3QuYmUwHQYDVR0OBBYEFD0IkfGG5ISacK+WD+nzNWo2XSKbMA0GCSqGSIb3DQEBCwUAA4IBAQAYkprXeAN5A6mIjm+hKI3pgvzsX3SKCaAcnBqhy4KtbWp81iOvWs1biDR1b9L/hzeNjCSNJ8kPJljApkcSAj4qjSj9GtRUCxgfxY5hT4SJiRyETwejhAptiiwAc//Zn8jZHV7qbnc73oqLTQMADUcalpdmbGR+kmexqFh9DwpDRrZ4AZxS658eZLD14821wtc72gH2yzd7Q8NlVJQYYKocWqqaso99A9ajD4lKPYWLW3Dfx86HBZTQ5PIo+l2mgpi+tTbZJQc92//kz8x4jzdpdgrykhDQlrct4lQXc4rUkb4kD+hOPuFTXB1rCXkIRO5/uuf0AXgHY1r3Jb6kAxeO"
            },
            function (profile, done) {

                User.update({ id: req.session.user.id }, {
                    nat_reg_number: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/privatepersonalidentifier'],
                    first_name: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname'],
                    last_name: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname'],
                    address: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/streetaddress']
                        + " " + profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/postalcode']
                        + " " + profile['be:fedict:eid:idp:pob'],
                    birth_date: profile['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/dateofbirth'],
                    confirmed: true
                }).then(() => { 
                    req.session.user = User.find({id:req.session.user.id});
                });
                return done();
            }));

        app.use(passport.initialize());

        app.get('/confirmUser',
            passport.authenticate('saml',
                {
                    successRedirect: '/',
                    failureRedirect: '/'
                }
            ));

        app.post('/confirmUser/callback',
            passport.authenticate('saml',
                {
                    successRedirect: '/login?email=' + req.session.user.auth.email + '&password=trolllll',
                    failureRedirect: '/'
                }
            ));


        return next();
    },
    
    getUserById:function(req,res){
        User.getUserById(req.session.user.id).then(function(result){
            if (!result) {
                res.view('403');
            } else {
                res.view('profile',{user:result});
            }
        });
    }

});

