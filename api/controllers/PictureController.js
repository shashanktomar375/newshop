/**
 * PictureController
 *
 * @description :: Server-side logic for managing Pictures
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getPicturesOf: function(req, res){
        const goodID = req.param('goodID');
        Picture.getPicturesOf(goodID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    addPictures: function(req, res){
        const uploadArray = req.param('uploadArray');
        const goodID = req.param('goodID');

        Picture.addPictures(uploadArray, goodID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    }
};

