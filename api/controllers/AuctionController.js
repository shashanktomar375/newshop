/**
 * AuctionController
 *
 * @description :: Server-side logic for managing Auctions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    getLastAuction: function(req, res){
        const id = req.param('id');
        Auction.getLastAuction(id).then(function (results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        });
    },

    getAllAuctions: function(req, res){
        const goodID = req.param('id');
        Auction.getAllAuctions(goodID).then(function(results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function(){
            res.view('500');
        })
    },

    getAuctionsByUser: function(req, res){
        const userID = req.param('id');
        var invalidAuctions = [];
        var failedAuctions = [];
        var successAuctions = [];
        var validAuctions = [];
        
        Auction.getAuctionsByUser(req.session.user.id).then(function(results){
            if(!results){
                res.view('404');
            }else{
                results.forEach(element => {
                    Good.getById(element.id_good).then((g)=>{
                        if(g.good_state != 'O'){
                            Auction.getLastAuction(element.id_good).then((lastAuct)=>{
                                if(lastAuct.id_user === element.id_user){
                                    successAuctions.push(element);
                                    return;
                                }else{
                                    failedAuctions.push(element);
                                    return;
                                }
                            });  
                        }else{
                            Auction.getLastAuction(element.id_good).then((lastAuct)=>{
                                if(lastAuct.id_user === element.id_user){
                                    validAuctions.push(element);
                                    return;
                                }else{
                                    invalidAuctions.push(element);
                                    return;
                                }
                            });  
                        }
                    });
                }); 
            }
        }).then(()=>{
            res.view('myauctions', {
                types :["warning","primary","success","danger"],
                titles:["en cours et invalides", "en cours et valides", "réussies", "échouées"],
                auctions:[invalidAuctions,validAuctions,successAuctions,failedAuctions]
            });
            console.log("it's ok");
        }).catch(function(){
            res.view('500');
        })
    },

    addAuction: function(req, res){
        const goodID = req.parem('goodID');
        const userID = req.param('userID');
        const price = req.param('price');
        //Récupère dernière enchère pour comparer prix
        Auction.getLastAuction(goodID).then(function(auction){
            //Si pas encore d'enchere
            if(!auction){
                return Auction.addAuction(goodId, userID, price);
            }else{
                //Vérifie prix > prix dernière enchère
                if(auction.price >= price){
                    //retourne un 400 - front doit afficher le msg erreur
                    res.badRequest("Le prix de la nouvelle enchère doit être supérieur à celui de la dernière enchère.");
                }else{
                    return Auction.addAuction(goodId, userID, price);
                }
            }
        }).then(function (results){
            if(!results){
                res.view('404');
            }else{
                res.ok(JSON.stringify(results));
            }
        }).catch(function (){
            res.view('500');
        });
    }
};

