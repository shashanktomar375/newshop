/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  'GET /': [
    function (req, res) {
      if (req.session.authenticated) {
        return res.redirect('/goods');
      } else {
        return res.redirect('/connect');
      }
    }
  ],



'/connect':{
  view: 'authentication',
  locals: {
    layout: false
  }
},

  

  '/login': [
    function (req, res, next) {
      if (req.session.authenticated) {
        return res.redirect('/goods');
      } else {
        return next();
      }
    }, {
      controller: 'AuthController',
      action: 'login',
    }],

    

  'GET /register': [
    function (req, res, next) {
      if (req.session.authenticated) {
        return res.redirect('/goods');
      } else {
        return next();
      }
    }, {
      view: 'register',
      locals: {
        layout: false
      }
    }],

  'POST /register': [
    function (req, res, next) {
      if (req.session.authenticated) {
        return res.redirect('/goods');
      } else {
        return next();
      }
    },
    {
      controller: 'AuthController',
      action: 'register',
    }],

  '/logout': {
    controller: 'AuthController',
    action: 'logout',
  },

  'POST /getConnectedUser': {
    controller: 'UserController',
    action: 'getUserById'
  },

  '/confirmUser': {
    controller: 'UserController',
    action: 'conf'
  },

  '/profile': {
    controller: 'UserController',
    action: 'getUserById'
  },

  'GET /goods': 'GoodController.getOnSale',

  'GET /goods/:categorie': {
    controller: 'GoodController',
    action: 'getGoodsByCategory'
  },

  '/settings' : {
    view : 'settings'
  },

  '/mygoods': {
    controller: 'GoodController',
    action: 'getGoodsByOwner'
  },

  '/myauctions': {
    controller: 'AuctionController',
    action: 'getAuctionsByUser'
  },
  '/categories': {
    controller: 'CategoryController',
    action: 'getAllCategories'
  },

  'GET /addGood': [
    function (req, res) {
      if (req.session.authenticated) {
        Category.getAllCategories().then(function (results) {
          return res.view('addgood', { categories: results });
        })
      } else {
        return res.redirect('/connect');
      }
    }],

  'POST /addGood': {
    controller: 'GoodController',
    action: 'addGood'
  },

  '/good/:id': {
    controller: 'GoodController',
    action: 'getById'
  },

  'GET /good/:name': 'GoodController.getByName',

};
